package caws

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/organizations"
	"github.com/aws/aws-sdk-go/service/sts"
)

// Default aws region for api calls that need a region
const constRegion = "eu-west-1"

// DisplayErrorInfo prints a message to stdout isErr equals true
func DisplayErrorInfo(isErr bool) {
	if isErr {
		fmt.Printf("\nErrors during run! Check your error.log\n")
	}
}

// WriteErrorLog writes messages to a file utilizing mutexes to manage concurrent writes
func WriteErrorLog(m string, f *os.File, w *sync.Mutex) {
	w.Lock()
	defer w.Unlock()

	f.WriteString(m)
	f.Sync()
}

// AccountBlacklisted returns true if account id is on blacklist
func AccountBlacklisted(account string, blacklist []string) bool {
	var result bool
	for _, element := range blacklist {
		if element == account {
			result = true
			break
		}
	}
	return result
}

// ReadBlacklist returns a slice for elements read from a file
func ReadBlacklist(path string) []string {
	var lines []string
	if len(path) != 0 {
		file, err := os.Open(path)
		CheckError("Cannot open blacklist\n", err)
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
	}
	return lines
}

// AssumeRoleOnSubAccount creates a new session by assuming a  role in a diffrent account.
// currentAcc is the id of the IAM account that was used to read the organization.
func AssumeRoleOnSubAccount(sess *session.Session, currentAcc string, acc string, role string, reg string) *session.Session {
	var newSess *session.Session
	if currentAcc != acc {
		roleToAssumeArn := fmt.Sprintf("arn:aws:iam::%v:role/%v", acc, role)
		creds := stscreds.NewCredentials(sess, roleToAssumeArn)
		newSess = sess.Copy(&aws.Config{
			Region:      aws.String(reg),
			Credentials: creds,
		})
	} else {
		newSess = sess.Copy(&aws.Config{
			Region: aws.String(reg),
		})
	}
	return newSess
}

// CreateRegionList returns a slice of strings, where each string is a valid AWS region
func CreateRegionList(sess *session.Session, region string) []string {
	var regions []string
	if len(region) <= 0 {
		regions = getRegions(sess)
		return regions
	}
	return []string{region}
}

func getRegions(sess *session.Session) []string {
	ec2Svc := ec2.New(sess)
	regions, err := ec2Svc.DescribeRegions(&ec2.DescribeRegionsInput{})
	CheckError("Cannot get regions\n", err)
	var result []string
	for _, region := range regions.Regions {
		result = append(result, *region.RegionName)
	}
	return result
}

// CreateSession returns a AWS session object. The session uses AWS shared credentials.
// If profile is set it will try to use a certain profile from the credentials file,
// otherwise it tries to read the environment variable AWS_PROFILE, than read the defaults
// profile from the credentials file.
func CreateSession(profile string) *session.Session {
	var sess *session.Session
	if len(profile) <= 0 {
		sess = session.Must(session.NewSession(&aws.Config{
			Region: aws.String(constRegion),
		}))
		return sess
	}
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(constRegion)},
		Profile: profile,
	}))
	return sess
}

// CreateAccountList gets all account from an AWS Organization and returns
// a slice of AWS ids.
func CreateAccountList(sess *session.Session, account string) []string {
	if len(account) <= 0 {
		return getOrganizationAccounts(sess)
	}
	return []string{account}
}

func getOrganizationAccounts(sess *session.Session) []string {
	orgSvc := organizations.New(sess)
	orgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{})
	CheckError("Cannot get organization accounts\n", err)
	token := orgAccounts.NextToken
	var accounts []string
	for _, account := range orgAccounts.Accounts {
		accounts = append(accounts, *account.Id)
	}

	for token != nil {
		moreOrgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{
			NextToken: token,
		})
		CheckError("Cannot fetch additional organization accounts\n", err)
		token = moreOrgAccounts.NextToken
		for _, account := range moreOrgAccounts.Accounts {
			accounts = append(accounts, *account.Id)
		}
	}
	return accounts
}

// CheckError prints message to stdout and calls os.Exit(1)
func CheckError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

// GetCurrentAccountID returns the accoutnid of the session used
func GetCurrentAccountID(sess *session.Session) string {
	stsSvc := sts.New(sess)
	idendtity, err := stsSvc.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	CheckError("Cannot get main account details\n", err)
	return *idendtity.Account
}
